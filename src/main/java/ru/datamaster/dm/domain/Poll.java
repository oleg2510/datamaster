package ru.datamaster.dm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * @auth velmisoff
 * Опрос
 */
@Entity
@Table(name = "poll")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Poll implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 3)
    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "date_start")
    private Instant dateStart;

    @Column(name = "date_finish")
    private Instant dateFinish;

    @Column(name = "activity")
    private Boolean activity;

    @OneToMany(mappedBy = "poll")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Question> questions = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Poll title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Instant getDateStart() {
        return dateStart;
    }

    public Poll dateStart(Instant dateStart) {
        this.dateStart = dateStart;
        return this;
    }

    public void setDateStart(Instant dateStart) {
        this.dateStart = dateStart;
    }

    public Instant getDateFinish() {
        return dateFinish;
    }

    public Poll dateFinish(Instant dateFinish) {
        this.dateFinish = dateFinish;
        return this;
    }

    public void setDateFinish(Instant dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Boolean isActivity() {
        return activity;
    }

    public Poll activity(Boolean activity) {
        this.activity = activity;
        return this;
    }

    public void setActivity(Boolean activity) {
        this.activity = activity;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public Poll questions(Set<Question> questions) {
        this.questions = questions;
        return this;
    }

    public Poll addQuestion(Question question) {
        this.questions.add(question);
        question.setPoll(this);
        return this;
    }

    public Poll removeQuestion(Question question) {
        this.questions.remove(question);
        question.setPoll(null);
        return this;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Poll)) {
            return false;
        }
        return id != null && id.equals(((Poll) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Poll{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", dateStart='" + getDateStart() + "'" +
            ", dateFinish='" + getDateFinish() + "'" +
            ", activity='" + isActivity() + "'" +
            "}";
    }
}
