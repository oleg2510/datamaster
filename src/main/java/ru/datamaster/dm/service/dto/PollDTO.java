package ru.datamaster.dm.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link ru.datamaster.dm.domain.Poll} entity.
 */
public class PollDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 3)
    private String title;

    private Instant dateStart;

    private Instant dateFinish;

    private Boolean activity;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Instant getDateStart() {
        return dateStart;
    }

    public void setDateStart(Instant dateStart) {
        this.dateStart = dateStart;
    }

    public Instant getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Instant dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Boolean isActivity() {
        return activity;
    }

    public void setActivity(Boolean activity) {
        this.activity = activity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PollDTO pollDTO = (PollDTO) o;
        if (pollDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pollDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PollDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", dateStart='" + getDateStart() + "'" +
            ", dateFinish='" + getDateFinish() + "'" +
            ", activity='" + isActivity() + "'" +
            "}";
    }
}
