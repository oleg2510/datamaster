package ru.datamaster.dm.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link ru.datamaster.dm.domain.Poll} entity. This class is used
 * in {@link ru.datamaster.dm.web.rest.PollResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /polls?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PollCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter title;

    private InstantFilter dateStart;

    private InstantFilter dateFinish;

    private BooleanFilter activity;

    private LongFilter questionId;

    public PollCriteria() {
    }

    public PollCriteria(PollCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.title = other.title == null ? null : other.title.copy();
        this.dateStart = other.dateStart == null ? null : other.dateStart.copy();
        this.dateFinish = other.dateFinish == null ? null : other.dateFinish.copy();
        this.activity = other.activity == null ? null : other.activity.copy();
        this.questionId = other.questionId == null ? null : other.questionId.copy();
    }

    @Override
    public PollCriteria copy() {
        return new PollCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public InstantFilter getDateStart() {
        return dateStart;
    }

    public void setDateStart(InstantFilter dateStart) {
        this.dateStart = dateStart;
    }

    public InstantFilter getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(InstantFilter dateFinish) {
        this.dateFinish = dateFinish;
    }

    public BooleanFilter getActivity() {
        return activity;
    }

    public void setActivity(BooleanFilter activity) {
        this.activity = activity;
    }

    public LongFilter getQuestionId() {
        return questionId;
    }

    public void setQuestionId(LongFilter questionId) {
        this.questionId = questionId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PollCriteria that = (PollCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(title, that.title) &&
            Objects.equals(dateStart, that.dateStart) &&
            Objects.equals(dateFinish, that.dateFinish) &&
            Objects.equals(activity, that.activity) &&
            Objects.equals(questionId, that.questionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        title,
        dateStart,
        dateFinish,
        activity,
        questionId
        );
    }

    @Override
    public String toString() {
        return "PollCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (dateStart != null ? "dateStart=" + dateStart + ", " : "") +
                (dateFinish != null ? "dateFinish=" + dateFinish + ", " : "") +
                (activity != null ? "activity=" + activity + ", " : "") +
                (questionId != null ? "questionId=" + questionId + ", " : "") +
            "}";
    }

}
