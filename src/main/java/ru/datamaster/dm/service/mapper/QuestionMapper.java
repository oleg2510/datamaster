package ru.datamaster.dm.service.mapper;


import ru.datamaster.dm.domain.*;
import ru.datamaster.dm.service.dto.QuestionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Question} and its DTO {@link QuestionDTO}.
 */
@Mapper(componentModel = "spring", uses = {PollMapper.class})
public interface QuestionMapper extends EntityMapper<QuestionDTO, Question> {

    @Mapping(source = "poll.id", target = "pollId")
    @Mapping(source = "poll.title", target = "pollTitle")
    QuestionDTO toDto(Question question);

    @Mapping(source = "pollId", target = "poll")
    Question toEntity(QuestionDTO questionDTO);

    default Question fromId(Long id) {
        if (id == null) {
            return null;
        }
        Question question = new Question();
        question.setId(id);
        return question;
    }
}
