package ru.datamaster.dm.service.mapper;


import ru.datamaster.dm.domain.*;
import ru.datamaster.dm.service.dto.PollDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Poll} and its DTO {@link PollDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PollMapper extends EntityMapper<PollDTO, Poll> {


    @Mapping(target = "questions", ignore = true)
    @Mapping(target = "removeQuestion", ignore = true)
    Poll toEntity(PollDTO pollDTO);

    default Poll fromId(Long id) {
        if (id == null) {
            return null;
        }
        Poll poll = new Poll();
        poll.setId(id);
        return poll;
    }
}
