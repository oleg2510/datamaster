/**
 * View Models used by Spring MVC REST controllers.
 */
package ru.datamaster.dm.web.rest.vm;
