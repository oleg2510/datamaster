package ru.datamaster.dm.web.rest;

import ru.datamaster.dm.DatamasterApp;
import ru.datamaster.dm.domain.Poll;
import ru.datamaster.dm.domain.Question;
import ru.datamaster.dm.repository.PollRepository;
import ru.datamaster.dm.service.PollService;
import ru.datamaster.dm.service.dto.PollDTO;
import ru.datamaster.dm.service.mapper.PollMapper;
import ru.datamaster.dm.web.rest.errors.ExceptionTranslator;
import ru.datamaster.dm.service.dto.PollCriteria;
import ru.datamaster.dm.service.PollQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static ru.datamaster.dm.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PollResource} REST controller.
 */
@SpringBootTest(classes = DatamasterApp.class)
public class PollResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATE_START = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_START = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_FINISH = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_FINISH = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_ACTIVITY = false;
    private static final Boolean UPDATED_ACTIVITY = true;

    @Autowired
    private PollRepository pollRepository;

    @Autowired
    private PollMapper pollMapper;

    @Autowired
    private PollService pollService;

    @Autowired
    private PollQueryService pollQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPollMockMvc;

    private Poll poll;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PollResource pollResource = new PollResource(pollService, pollQueryService);
        this.restPollMockMvc = MockMvcBuilders.standaloneSetup(pollResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Poll createEntity(EntityManager em) {
        Poll poll = new Poll()
            .title(DEFAULT_TITLE)
            .dateStart(DEFAULT_DATE_START)
            .dateFinish(DEFAULT_DATE_FINISH)
            .activity(DEFAULT_ACTIVITY);
        return poll;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Poll createUpdatedEntity(EntityManager em) {
        Poll poll = new Poll()
            .title(UPDATED_TITLE)
            .dateStart(UPDATED_DATE_START)
            .dateFinish(UPDATED_DATE_FINISH)
            .activity(UPDATED_ACTIVITY);
        return poll;
    }

    @BeforeEach
    public void initTest() {
        poll = createEntity(em);
    }

    @Test
    @Transactional
    public void createPoll() throws Exception {
        int databaseSizeBeforeCreate = pollRepository.findAll().size();

        // Create the Poll
        PollDTO pollDTO = pollMapper.toDto(poll);
        restPollMockMvc.perform(post("/api/polls")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pollDTO)))
            .andExpect(status().isCreated());

        // Validate the Poll in the database
        List<Poll> pollList = pollRepository.findAll();
        assertThat(pollList).hasSize(databaseSizeBeforeCreate + 1);
        Poll testPoll = pollList.get(pollList.size() - 1);
        assertThat(testPoll.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testPoll.getDateStart()).isEqualTo(DEFAULT_DATE_START);
        assertThat(testPoll.getDateFinish()).isEqualTo(DEFAULT_DATE_FINISH);
        assertThat(testPoll.isActivity()).isEqualTo(DEFAULT_ACTIVITY);
    }

    @Test
    @Transactional
    public void createPollWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pollRepository.findAll().size();

        // Create the Poll with an existing ID
        poll.setId(1L);
        PollDTO pollDTO = pollMapper.toDto(poll);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPollMockMvc.perform(post("/api/polls")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pollDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Poll in the database
        List<Poll> pollList = pollRepository.findAll();
        assertThat(pollList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = pollRepository.findAll().size();
        // set the field null
        poll.setTitle(null);

        // Create the Poll, which fails.
        PollDTO pollDTO = pollMapper.toDto(poll);

        restPollMockMvc.perform(post("/api/polls")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pollDTO)))
            .andExpect(status().isBadRequest());

        List<Poll> pollList = pollRepository.findAll();
        assertThat(pollList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPolls() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList
        restPollMockMvc.perform(get("/api/polls?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(poll.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].dateStart").value(hasItem(DEFAULT_DATE_START.toString())))
            .andExpect(jsonPath("$.[*].dateFinish").value(hasItem(DEFAULT_DATE_FINISH.toString())))
            .andExpect(jsonPath("$.[*].activity").value(hasItem(DEFAULT_ACTIVITY.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getPoll() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get the poll
        restPollMockMvc.perform(get("/api/polls/{id}", poll.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(poll.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.dateStart").value(DEFAULT_DATE_START.toString()))
            .andExpect(jsonPath("$.dateFinish").value(DEFAULT_DATE_FINISH.toString()))
            .andExpect(jsonPath("$.activity").value(DEFAULT_ACTIVITY.booleanValue()));
    }


    @Test
    @Transactional
    public void getPollsByIdFiltering() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        Long id = poll.getId();

        defaultPollShouldBeFound("id.equals=" + id);
        defaultPollShouldNotBeFound("id.notEquals=" + id);

        defaultPollShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPollShouldNotBeFound("id.greaterThan=" + id);

        defaultPollShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPollShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllPollsByTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where title equals to DEFAULT_TITLE
        defaultPollShouldBeFound("title.equals=" + DEFAULT_TITLE);

        // Get all the pollList where title equals to UPDATED_TITLE
        defaultPollShouldNotBeFound("title.equals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllPollsByTitleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where title not equals to DEFAULT_TITLE
        defaultPollShouldNotBeFound("title.notEquals=" + DEFAULT_TITLE);

        // Get all the pollList where title not equals to UPDATED_TITLE
        defaultPollShouldBeFound("title.notEquals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllPollsByTitleIsInShouldWork() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where title in DEFAULT_TITLE or UPDATED_TITLE
        defaultPollShouldBeFound("title.in=" + DEFAULT_TITLE + "," + UPDATED_TITLE);

        // Get all the pollList where title equals to UPDATED_TITLE
        defaultPollShouldNotBeFound("title.in=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllPollsByTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where title is not null
        defaultPollShouldBeFound("title.specified=true");

        // Get all the pollList where title is null
        defaultPollShouldNotBeFound("title.specified=false");
    }
                @Test
    @Transactional
    public void getAllPollsByTitleContainsSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where title contains DEFAULT_TITLE
        defaultPollShouldBeFound("title.contains=" + DEFAULT_TITLE);

        // Get all the pollList where title contains UPDATED_TITLE
        defaultPollShouldNotBeFound("title.contains=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllPollsByTitleNotContainsSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where title does not contain DEFAULT_TITLE
        defaultPollShouldNotBeFound("title.doesNotContain=" + DEFAULT_TITLE);

        // Get all the pollList where title does not contain UPDATED_TITLE
        defaultPollShouldBeFound("title.doesNotContain=" + UPDATED_TITLE);
    }


    @Test
    @Transactional
    public void getAllPollsByDateStartIsEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where dateStart equals to DEFAULT_DATE_START
        defaultPollShouldBeFound("dateStart.equals=" + DEFAULT_DATE_START);

        // Get all the pollList where dateStart equals to UPDATED_DATE_START
        defaultPollShouldNotBeFound("dateStart.equals=" + UPDATED_DATE_START);
    }

    @Test
    @Transactional
    public void getAllPollsByDateStartIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where dateStart not equals to DEFAULT_DATE_START
        defaultPollShouldNotBeFound("dateStart.notEquals=" + DEFAULT_DATE_START);

        // Get all the pollList where dateStart not equals to UPDATED_DATE_START
        defaultPollShouldBeFound("dateStart.notEquals=" + UPDATED_DATE_START);
    }

    @Test
    @Transactional
    public void getAllPollsByDateStartIsInShouldWork() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where dateStart in DEFAULT_DATE_START or UPDATED_DATE_START
        defaultPollShouldBeFound("dateStart.in=" + DEFAULT_DATE_START + "," + UPDATED_DATE_START);

        // Get all the pollList where dateStart equals to UPDATED_DATE_START
        defaultPollShouldNotBeFound("dateStart.in=" + UPDATED_DATE_START);
    }

    @Test
    @Transactional
    public void getAllPollsByDateStartIsNullOrNotNull() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where dateStart is not null
        defaultPollShouldBeFound("dateStart.specified=true");

        // Get all the pollList where dateStart is null
        defaultPollShouldNotBeFound("dateStart.specified=false");
    }

    @Test
    @Transactional
    public void getAllPollsByDateFinishIsEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where dateFinish equals to DEFAULT_DATE_FINISH
        defaultPollShouldBeFound("dateFinish.equals=" + DEFAULT_DATE_FINISH);

        // Get all the pollList where dateFinish equals to UPDATED_DATE_FINISH
        defaultPollShouldNotBeFound("dateFinish.equals=" + UPDATED_DATE_FINISH);
    }

    @Test
    @Transactional
    public void getAllPollsByDateFinishIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where dateFinish not equals to DEFAULT_DATE_FINISH
        defaultPollShouldNotBeFound("dateFinish.notEquals=" + DEFAULT_DATE_FINISH);

        // Get all the pollList where dateFinish not equals to UPDATED_DATE_FINISH
        defaultPollShouldBeFound("dateFinish.notEquals=" + UPDATED_DATE_FINISH);
    }

    @Test
    @Transactional
    public void getAllPollsByDateFinishIsInShouldWork() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where dateFinish in DEFAULT_DATE_FINISH or UPDATED_DATE_FINISH
        defaultPollShouldBeFound("dateFinish.in=" + DEFAULT_DATE_FINISH + "," + UPDATED_DATE_FINISH);

        // Get all the pollList where dateFinish equals to UPDATED_DATE_FINISH
        defaultPollShouldNotBeFound("dateFinish.in=" + UPDATED_DATE_FINISH);
    }

    @Test
    @Transactional
    public void getAllPollsByDateFinishIsNullOrNotNull() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where dateFinish is not null
        defaultPollShouldBeFound("dateFinish.specified=true");

        // Get all the pollList where dateFinish is null
        defaultPollShouldNotBeFound("dateFinish.specified=false");
    }

    @Test
    @Transactional
    public void getAllPollsByActivityIsEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where activity equals to DEFAULT_ACTIVITY
        defaultPollShouldBeFound("activity.equals=" + DEFAULT_ACTIVITY);

        // Get all the pollList where activity equals to UPDATED_ACTIVITY
        defaultPollShouldNotBeFound("activity.equals=" + UPDATED_ACTIVITY);
    }

    @Test
    @Transactional
    public void getAllPollsByActivityIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where activity not equals to DEFAULT_ACTIVITY
        defaultPollShouldNotBeFound("activity.notEquals=" + DEFAULT_ACTIVITY);

        // Get all the pollList where activity not equals to UPDATED_ACTIVITY
        defaultPollShouldBeFound("activity.notEquals=" + UPDATED_ACTIVITY);
    }

    @Test
    @Transactional
    public void getAllPollsByActivityIsInShouldWork() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where activity in DEFAULT_ACTIVITY or UPDATED_ACTIVITY
        defaultPollShouldBeFound("activity.in=" + DEFAULT_ACTIVITY + "," + UPDATED_ACTIVITY);

        // Get all the pollList where activity equals to UPDATED_ACTIVITY
        defaultPollShouldNotBeFound("activity.in=" + UPDATED_ACTIVITY);
    }

    @Test
    @Transactional
    public void getAllPollsByActivityIsNullOrNotNull() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where activity is not null
        defaultPollShouldBeFound("activity.specified=true");

        // Get all the pollList where activity is null
        defaultPollShouldNotBeFound("activity.specified=false");
    }

    @Test
    @Transactional
    public void getAllPollsByQuestionIsEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);
        Question question = QuestionResourceIT.createEntity(em);
        em.persist(question);
        em.flush();
        poll.addQuestion(question);
        pollRepository.saveAndFlush(poll);
        Long questionId = question.getId();

        // Get all the pollList where question equals to questionId
        defaultPollShouldBeFound("questionId.equals=" + questionId);

        // Get all the pollList where question equals to questionId + 1
        defaultPollShouldNotBeFound("questionId.equals=" + (questionId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPollShouldBeFound(String filter) throws Exception {
        restPollMockMvc.perform(get("/api/polls?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(poll.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].dateStart").value(hasItem(DEFAULT_DATE_START.toString())))
            .andExpect(jsonPath("$.[*].dateFinish").value(hasItem(DEFAULT_DATE_FINISH.toString())))
            .andExpect(jsonPath("$.[*].activity").value(hasItem(DEFAULT_ACTIVITY.booleanValue())));

        // Check, that the count call also returns 1
        restPollMockMvc.perform(get("/api/polls/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPollShouldNotBeFound(String filter) throws Exception {
        restPollMockMvc.perform(get("/api/polls?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPollMockMvc.perform(get("/api/polls/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPoll() throws Exception {
        // Get the poll
        restPollMockMvc.perform(get("/api/polls/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePoll() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        int databaseSizeBeforeUpdate = pollRepository.findAll().size();

        // Update the poll
        Poll updatedPoll = pollRepository.findById(poll.getId()).get();
        // Disconnect from session so that the updates on updatedPoll are not directly saved in db
        em.detach(updatedPoll);
        updatedPoll
            .title(UPDATED_TITLE)
            .dateStart(UPDATED_DATE_START)
            .dateFinish(UPDATED_DATE_FINISH)
            .activity(UPDATED_ACTIVITY);
        PollDTO pollDTO = pollMapper.toDto(updatedPoll);

        restPollMockMvc.perform(put("/api/polls")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pollDTO)))
            .andExpect(status().isOk());

        // Validate the Poll in the database
        List<Poll> pollList = pollRepository.findAll();
        assertThat(pollList).hasSize(databaseSizeBeforeUpdate);
        Poll testPoll = pollList.get(pollList.size() - 1);
        assertThat(testPoll.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testPoll.getDateStart()).isEqualTo(UPDATED_DATE_START);
        assertThat(testPoll.getDateFinish()).isEqualTo(UPDATED_DATE_FINISH);
        assertThat(testPoll.isActivity()).isEqualTo(UPDATED_ACTIVITY);
    }

    @Test
    @Transactional
    public void updateNonExistingPoll() throws Exception {
        int databaseSizeBeforeUpdate = pollRepository.findAll().size();

        // Create the Poll
        PollDTO pollDTO = pollMapper.toDto(poll);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPollMockMvc.perform(put("/api/polls")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pollDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Poll in the database
        List<Poll> pollList = pollRepository.findAll();
        assertThat(pollList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePoll() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        int databaseSizeBeforeDelete = pollRepository.findAll().size();

        // Delete the poll
        restPollMockMvc.perform(delete("/api/polls/{id}", poll.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Poll> pollList = pollRepository.findAll();
        assertThat(pollList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
