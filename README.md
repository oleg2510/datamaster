# Тестовое задание для Data master

src/main/resource/config/liquibase - конфа для миграции (использую faker в dev профиле для обогощения данными и демо)

src/main/domain - бизнес сущности

вся кодовая база сделанна по best practice

### Quick start (на H2 в dev профиле)

    ./gradlew

### Quick start (на Postgres в prod профиле)

    ./gradlew -Pprod, swagger

## Тестирование

    ./gradlew test integrationTest jacocoTestReport

### Качество кода (покрыт6ие 86%)

Сонар используется для анализа качества кода. Вы можете запустить локальный сервер Sonar (доступный по адресу http://localhost: 9001) с помощью::

```
docker-compose -f src/main/docker/sonar.yml up -d
```

```
./gradlew -Pprod clean check jacocoTestReport sonarqube
```

## Юзаем Docker для упрощения разработки (опционально)

    docker-compose -f src/main/docker/postgresql.yml up -d

Остановить через орекстратор БД:

    docker-compose -f src/main/docker/postgresql.yml down

Контейнерезация аппы

    ./gradlew bootJar -Pprod jibDockerBuild
